/*
 * Autor       : Mikael Nilsson
 * Filename    : main.cpp
 * Description : C++ Screen example
 * Version     : 0.2
 *
*/

#include <memstat.hpp>
#include "screen.h"
#include <iostream>

using namespace std;

int main()
{
    Terminal terminal;
    Screen screen(80, 24);

    screen.fill(' ', TerminalColor(COLOR::BLACK, COLOR::WHITE));
    screen.fillRect(35, 9, 9, 1, ' ', TerminalColor(COLOR::BLUE, COLOR::BLUE));
    screen.fillRect(35, 8, 10, 1, ' ', TerminalColor(COLOR::BLUE, COLOR::BLUE));
    screen.fillRect(38, 7, 3, 1, ' ', TerminalColor(COLOR::BLUE, COLOR::BLUE));
    screen.fillRect(42, 7, 3, 1, ' ', TerminalColor(COLOR::BLUE, COLOR::BLUE));
    screen.set(40,6,' ',TerminalColor(COLOR::BLUE, COLOR::BLUE));
    screen.set(44,6,' ',TerminalColor(COLOR::BLUE, COLOR::BLUE));

    screen.fillRect(36, 4, 9, 1, ' ', TerminalColor(COLOR::YELLOW, COLOR::YELLOW));
    screen.fillRect(35, 5, 5, 1, ' ', TerminalColor(COLOR::YELLOW, COLOR::YELLOW));
    screen.fillRect(41, 5, 3, 1, ' ', TerminalColor(COLOR::YELLOW, COLOR::YELLOW));
    screen.fillRect(35, 6, 4, 1, ' ', TerminalColor(COLOR::YELLOW, COLOR::YELLOW));
    screen.set(42,6,' ',TerminalColor(COLOR::YELLOW, COLOR::YELLOW));
    screen.fillRect(35, 7, 2, 1, ' ', TerminalColor(COLOR::YELLOW, COLOR::YELLOW));

    screen.setTextRect(31,11,17,1, "MittUniversitetet", TerminalColor(COLOR::BLACK, COLOR::WHITE));
    screen.setText(29, 12,string("Mid Sweden University"),TerminalColor(COLOR::BLACK, COLOR::WHITE));

    screen.draw(terminal);

    return 0;
}

